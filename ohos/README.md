# file_preview_ohos

文本预览插件。

file_preview 在 OpenHarmony 平台的实现。

## Usage

```yaml
dependencies:
  file_preview: 1.2.0
  file_preview_ohos: 1.0.0
```

## Example:

```dart
import 'package:file_preview_ohos/file_preview.dart';

//文件控制器
FilePreviewController controller = FilePreviewController();

//切换文件
//path 文件地址 https/http开头、文件格式结尾的地址，或者本地绝对路径
controller.showFile(path);

//文件预览widget
FilePreviewWidget(
    controller: controller,
    width: 400,
    height: 600,
    //path 文件地址 https/http开头、文件格式结尾的地址，或者本地绝对路径
    path: widget.path,
    callBack: FilePreviewCallBack(onShow: () {
      print("文件打开成功");
    }, onDownload: (progress) {
      print("文件下载进度$progress");
    }, onFail: (code, msg) {
      print("文件打开失败 $code  $msg");
    }),
),
```

## 支持格式
|格式|android|ios|   harmonyOS    |
|:----|:----:|:----:|:--------------:|
|.doc| ✅ | ✅ |       ❌        |
|.docx| ✅ | ✅ |       ❌        |
|.ppt| ✅ | ✅ |       ❌        |
|.pptx| ✅ | ✅ |       ❌        |
|.xls| ✅ | ✅ |       ❌        |
|.xlsx| ✅ | ✅ |       ❌        |
|.pdf|✅ | ✅ |       ✅        |
|.OFD|✅ | ✅ |       ❌        |
|more| TBS限制不可预览 | WKWebView支持均可预览 | 本地预览仅支持txt和pdf |