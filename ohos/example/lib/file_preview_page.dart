import 'package:file_preview_ohos/file_preview.dart';
import 'package:flutter/material.dart';

import 'main.dart';

/// @Author: gstory
/// @CreateDate: 2021/12/27 10:27 上午
/// @Email gstory0404@gmail.com
/// @Description: dart类作用描述

class FilePreviewPage extends StatefulWidget {
  final String title;
  final String path;
  final String page;
  final AssetsPath? assetsPath;

  const FilePreviewPage({Key? key, required this.path, required this.title, required this.page, this.assetsPath})
      : super(key: key);

  @override
  _FilePreviewPageState createState() => _FilePreviewPageState();
}

class _FilePreviewPageState extends State<FilePreviewPage> {
  FilePreviewController controller = FilePreviewController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Container(
            alignment: Alignment.topLeft,
            child: FilePreviewWidget(
             controller: controller,
              width: 400,
              height: 600,
              path: widget.path,
              callBack: FilePreviewCallBack(onShow: () {
                print("文件打开成功");
              }, onDownload: (progress) {
                print("文件下载进度$progress");
              }, onFail: (code, msg) {
                print("文件打开失败 $code  $msg");
              }),
            ),
          ),
          Row(
            children: [
              if (widget.page == 'Web')
              TextButton(
                onPressed: () {
                  controller.showFile("http://vw.usdoc.cn/?m=5&src=http://usdoc.cn/vw/%E6%96%87%E4%BB%B6%E6%A8%A1%E6%9D%BF.docx");
                },
                child: Text("docx"),
              ),
              TextButton(
                onPressed: () {
                  if (widget.page == 'Web') {
                    controller.showFile("https://pdfkit.org/docs/guide.pdf");
                  } else if (widget.page == 'Assets') {
                    controller.showFile(widget.assetsPath?.pathPDF ?? "");
                  }
                },
                child: Text("pdf"),
              ),
              if (widget.page == 'Web')
              TextButton(
                onPressed: () {
                  controller.showFile("https://gstory.vercel.app/ceshi/ceshi.xisx");
                },
                child: Text("xisx"),
              ),
              TextButton(
                onPressed: () {
                  if (widget.page == 'Web') {
                    controller.showFile("https://gstory.vercel.app/ceshi/ceshi.txt");
                  } else if (widget.page == 'Assets') {
                    controller.showFile(widget.assetsPath?.pathTXT ?? "");
                  }
                },
                child: Text("txt"),
              ),
              if (widget.page == 'Web')
              TextButton(
                onPressed: () {
                  controller.showFile("https://gstory.vercel.app/ceshi/ceshi.pptx");
                },
                child: Text("ppt"),
              ),
            ],
          )
        ],
      ),
    );
  }
}
