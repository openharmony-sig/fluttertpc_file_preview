import 'dart:io';
import 'dart:typed_data';
import 'dart:async';

import 'package:path_provider/path_provider.dart';
import 'package:file_preview_example/file_preview_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:file_preview_ohos/file_preview.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class AssetsPath {
  String pathTXT = "";
  String pathPDF = "";
}

class _HomePageState extends State<HomePage> {

  var assetsPath = AssetsPath();

  @override
  void initState() {
    super.initState();
    fromAsset('assets/file_test.txt', 'file_test.txt').then((f) {
      setState(() {
        assetsPath.pathTXT = f.path;
      });
    });
    fromAsset('assets/file_test.pdf', 'file_test.pdf').then((f) {
      setState(() {
        assetsPath.pathPDF = f.path;
      });
    });
  }

  Future<File> fromAsset(String asset, String filename) async {
    // To open from assets, you can copy them to the app storage folder, and the access them "locally"
    Completer<File> completer = Completer();
    try {
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/$filename");
      var data = await rootBundle.load(asset);
      var bytes = data.buffer.asUint8List();
      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('File Preview'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
              color: Colors.blue,
              textColor: Colors.white,
              child: const Text('在线预览'),
              onPressed: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) {
                      return const FilePreviewPage(
                        page: "Web",
                        title: "网络预览",
                        path: "http://vw.usdoc.cn/?m=5&src=http://usdoc.cn/vw/%E6%96%87%E4%BB%B6%E6%A8%A1%E6%9D%BF.docx",
                      );
                    },
                  ),
                );
              },
            ),
            MaterialButton(
              color: Colors.blue,
              textColor: Colors.white,
              child: const Text('Assets文件预览'),
              onPressed: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_){
                      return FilePreviewPage(
                        page: 'Assets',
                        title: "本地文件预览",
                        path: assetsPath.pathPDF,
                        assetsPath: assetsPath,
                      );
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
