/*
* Copyright (c) 2024 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import { Params } from '@ohos/flutter_ohos/src/main/ets/plugin/platform/PlatformView';
import { webview } from '@kit.ArkWeb';
import { FilePreview } from './FilePreview'
import { common } from '@kit.AbilityKit';

const MASKS: string = "#toolbar=0";
const HEADER: string = "file://";

@Entry
@Component
struct WebView {
  @Prop params: Params;
  @State url: string = '';
  controller: webview.WebviewController = new webview.WebviewController();
  filePreview: FilePreview = this.params.platformView as FilePreview

  getUrl(url: string) {
    if (url.slice(0, 4) === 'http') {
      return url + MASKS;
    } else {
      return HEADER + url + MASKS;
    }
  }

  aboutToAppear(): void {
    this.url = this.getUrl(this.filePreview.getFilePath());

    let context = getContext() as common.UIAbilityContext;
    context.eventHub.on('showFile', (filePath: string) => {
      this.controller.loadUrl(this.getUrl(filePath));
    });
  }

  build() {
    Column() {
      Web({ src: this.url, controller: this.controller })
        .fileAccess(true)
        .domStorageAccess(true)
        .onPageBegin((event) => {
          if (event) {
            console.log('file开始加载');
          }
        })
        .onPageEnd((event) => {
          if (event) {
            console.log('file加载完成');
            this.filePreview.onShow();
          }
        })
        .onErrorReceive((event) => {
          if (event) {
            console.error('file加载失败');
            let map: Map<string, string> = new Map();
            map.set('code:', event.error.getErrorInfo());
            map.set('msg:', event.error.getErrorInfo());
            this.filePreview.onFail(map);
          }
        })
    }
  }
}

@Builder
export function WebBuilder(params: Params) {
  WebView({ params: params });
}