/*
* Copyright (c) 2024 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import 'package:file_preview_ohos/file_preview.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFilePreview
    with MockPlatformInterfaceMixin
    implements FilePreview {

  Future<bool> deleteCache() async {
    return true;
  }

  Future<bool> tbsHasInit() async {
    return true;
  }

  Future<String> tbsVersion() async {
    return '1.0.0';
  }

}

void main() {
  MockFilePreview view = MockFilePreview();

  test('deleteCache', () async {
    expect(await view.deleteCache(), true);
  });

  test('tbsHasInit', () async {
    expect(await view.tbsHasInit(), true);
  });

  test('tbsVersion', () async {
    expect(await view.tbsVersion(), '1.0.0');
  });
}